import { Component, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';

@Component({
  selector: 'app-apollo-client',
  templateUrl: './apollo-client.component.html',
  styleUrls: ['./apollo-client.component.scss']
})
export class ApolloClientComponent implements OnInit {

  books: any[] = [];
  loading = true;
  error: any;

  constructor(private apollo: Apollo) { }

  ngOnInit(): void {
    this.apollo
      .watchQuery({
        query: gql`
        {
          books {
            title
            author
          }
        }
      `,
      })
      .valueChanges.subscribe((result: any) => {
        this.books = result?.data?.books;
        this.loading = result.loading;
        this.error = result.error;
      });
  }
}
